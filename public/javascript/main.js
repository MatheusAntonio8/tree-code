// Global Tree 
var tree = {};
// Node's index
var idCounter = 0;
// Environment settings
var config = {
    showConfig: false,
    alerts: false,
    qtdNodes: 10,
    range: 100,
    showNodeLevel: true,
}

function insertNode(level, ref, tree, value) {

    if (tree.value != null) {
        if (tree.value === value) {
            if (config.alertsalerts)
                alert(`O valor ${value} já esta presente na árvore!`);
            return;
        } else
            insertNode(++level, tree.ref, (tree.value > value ? tree.left : tree.right), value);
    } else {
        idCounter++
        tree.id = idCounter;
        tree.value = value;
        tree.left = {};
        tree.right = {};

        if (ref === null)
            tree.ref = {
                position: 'root',
                value: value,
                top: 100,
                left: -25,
                level: level
            }

        else
            tree.ref = {
                position: (ref.value > tree.value ? 'left' : 'right'),
                value: value,
                top: ref.top + 70,
                left: (ref.value > tree.value ? ref.left - 100 : ref.left + 100),
                level: level
            };

        drawNode(value, tree.id, tree.ref);
    }
}

function drawNode(value, id, ref) {
    const container = document.getElementById('treeContainer');
    let newNode = document.createElement('div');
    let newNodeLine = document.createElement('div');
    let small = document.createElement('small');
    if (ref.position != 'root') {
        newNodeLine.className = ref.position === 'left' ? 'nodeline_left' : 'nodeline_right';
        newNodeLine.style.top = `${ref.top - 50}px`;
        newNodeLine.style.left = `calc(50vw + ${ref.position === 'left' ? (ref.left + 25) : (ref.left - 50)}px)`;
        container.appendChild(newNodeLine);
    }
    newNode.className = 'node';
    newNode.id = id;
    newNode.innerText = value;
    small.style.display = config.showNodeLevel ? 'initial' : 'none';
    small.innerText = ref.level;
    small.className = 'level';
    newNode.appendChild(small);
    newNode.style.top = `${ref.top}px`;
    newNode.style.left = `calc(50vw + ${ref.left}px)`;
    container.appendChild(newNode);
}

function appendNode() {
    config.alerts = true;
    let value = input.value;
    if (value) {
        if (value > 1000 || value < -1000)
            alert('Somente números entre -1000 a 1000');

        else {
            input.value = '';
            insertNode(0, (tree.value ? tree.ref : null), tree, parseInt(value));
            console.log('Árvore', tree);
        }
    }
}

function createRandomTree() {
    idCounter = 0;
    config.alerts = false;
    removeTree();
    let value = 0;
    for (let i = 0; i < config.qtdNodes; i++) {
        value = Math.floor(Math.random() * config.range);
        insertNode(0, (tree.value ? tree.ref : null), tree, parseInt(value));
    }
}

function removeTree() {
    const container = document.getElementById('container');
    const treeContainer = document.getElementById('treeContainer');
    container.removeChild(treeContainer);
    let NewContainer = document.createElement('div');
    NewContainer.id = 'treeContainer';
    container.appendChild(NewContainer);
    tree = {};
}

function search(ids, value, tree) {
    if (tree === undefined) {
        alert('Não encontrado...');
        return;
    }

    if (tree.value === value) {
        document.getElementById(tree.id).className = 'nodeFind';
        ids.forEach(Element => {
            let node = document.getElementById(Element);
            node.className = 'nodeFindWay';
        });
    } else if (tree.value > value) {
        ids.push(tree.id);
        search(ids, value, tree.left);
    } else {
        ids.push(tree.id);
        search(ids, value, tree.right);
    }
}

function showConfigElement() {
    config.showConfig = !config.showConfig;
    let configElement = document.getElementById('config');
    let menuElement = document.getElementById('menu');

    configElement.style.display = config.showConfig ? 'initial' : 'none';
    menuElement.style.display = config.showConfig ? 'none' : 'initial';
}

function saveConfig() {
    let qtdNodes = parseInt(document.getElementById('maxQtd').value);
    let maxValue = parseInt(document.getElementById('maxValue').value);
    let cb = document.getElementById('cbLevel').checked;

    if(qtdNodes <= 1000 && qtdNodes >= 3)
        config.qtdNodes = qtdNodes;
    else {
        alert('a quantidade de nodes deve ser entre 0 a 1000!');
        config.qtdNodes = 15;
    }


    if (maxValue <= 1000 && maxValue >= 10)
        config.range = maxValue;
    else{
        alert('O valor dos nodes devem estar entre 0 a 1000!');
        config.range = 100;
    } 
    config.showNodeLevel = cb;

    showLevel(cb);
    showConfigElement();
}

function getValues(vet, tree) {
    if (tree.value == undefined)
        return;

    vet.push(tree.value);
    getValues(vet, tree.left);
    getValues(vet, tree.right);
}

function compararNumeros(a, b) {
    return a - b;
}

function orderTree(tree) {
    let vet = [];

    getValues(vet, tree);

    vet = vet.sort(compararNumeros);

    vet.forEach(console.log);
}

function showLevel(flag) {
    Array.from(document.querySelectorAll('.level'))
        .forEach(Element => Element.style.display = (flag ? 'initial' : 'none'))
}

// Button Add click Event 
btn_add.addEventListener('click', appendNode);

// Button generate Tree Event 
btn_randomTree.addEventListener('click', createRandomTree);

// Button remove Tree Event
btn_removeTree.addEventListener('click', removeTree);

// Button save config Event
btn_save_config.addEventListener('click', saveConfig);

// Button search value Event
btn_search.addEventListener('click', () => {
    Array.from(document.querySelectorAll('.nodeFind'))
        .forEach(Element => Element.className = 'node');
    Array.from(document.querySelectorAll('.nodeFindWay'))
        .forEach(Element => Element.className = 'node');
    if (parseInt(input.value))
        search([], parseInt(input.value), tree);
})